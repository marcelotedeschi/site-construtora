<?php

include 'dbSettings.php';

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);
// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

$sql = "SELECT id, titulo, subtitulo, active, url FROM portfolio, foto WHERE preview = 1 AND portfolio.id = idPortfolio AND active = FALSE ";
$result = $conn->query($sql);

if ($result->num_rows > 0) {
    // output data of each row
    while($row = $result->fetch_assoc()) {
        if ($row["active"] == 0) { ?>
            <div class="col-md-4 col-sm-6 portfolio-item">
                <a href="#portfolioModal<?php echo $row["id"]; ?>" class="portfolio-link" data-toggle="modal">
                    <div class="portfolio-hover">
                        <div class="portfolio-hover-content">
                            <i class="fa fa-plus fa-3x"></i>
                        </div>
                    </div>
                    <img src="<?php echo $row["url"]; ?>" class="img-responsive" alt="">
                </a>

                <div class="portfolio-caption">
                    <h4><?php echo $row["titulo"]; ?></h4>

                    <p class="text-muted"><?php echo $row["subtitulo"]; ?></p>
                </div>
            </div>
            <?php
        }
    }
}
$conn->close();
?>

