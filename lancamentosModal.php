<?php
// Database settings
include 'dbSettings.php';

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);
// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

//$sql = "SELECT * FROM portfolio, foto WHERE id = idPortfolio AND preview = 0";
$sql = 'SELECT * FROM portfolio WHERE active = TRUE';
$resultPortfolio = $conn->query($sql);


if ($resultPortfolio->num_rows > 0) {
    // output data of each row
    while($rowPortfolio = $resultPortfolio->fetch_assoc()) {?>
        <div class="lancamentos-modal modal fade" id="lancamentosModal<?php echo $rowPortfolio["id"]; ?>" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-content">
                <div class="close-modal" data-dismiss="modal">
                    <div class="lr">
                        <div class="rl">
                        </div>
                    </div>
                </div>
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-8 col-lg-offset-2">
                            <div class="modal-body">
                                <!-- Project Details Go Here -->
                                <h2><?php echo $rowPortfolio["titulo"]; ?></h2>
                                <p class="item-intro text-muted"><?php echo $rowPortfolio["subtitulo"];?></p>
                                <div id="myCarousel<?php echo $rowPortfolio["id"]; ?>" class="carousel slide" data-ride="carousel">
                                    <!-- Indicators -->
                                    <?php
                                    $sql2 = "SELECT * FROM foto WHERE preview = 0 AND idPortfolio = " . (int)$rowPortfolio["id"];
                                    $resultFoto = $conn->query($sql2);
                                    $resultFoto2 = $conn->query($sql2);
                                    ?>
                                    <ol class="carousel-indicators">
                                        <?php
                                        if ($resultFoto->num_rows > 0 ){
                                            $count = 0;
                                            while ($rowFoto = $resultFoto->fetch_assoc()) {
                                                if ($count == 0) {?>
                                                    <li data-target="#myCarousel<?php echo $rowPortfolio["id"]; ?>" data-slide-to="<?php echo $count;?>" class="active"></li>
                                                <?php } else {?>
                                                    <li data-target="#myCarousel<?php echo $rowPortfolio["id"]; ?>" data-slide-to="<?php echo $count;?>"></li>
                                                <?php }
                                                $count++;
                                            }
                                        } ?>
                                    </ol>
                                    <!-- Wrapper for slides -->
                                    <div class="carousel-inner" role="listbox">
                                        <?php
                                        if ($resultFoto2->num_rows > 0 ){
                                            $active = false;
                                            while ($rowFoto2 = $resultFoto2->fetch_assoc()) {
                                                if ($active == true) {?>
                                                    <div class="item imgcarousel">
                                                        <img class="" src="<?php echo $rowFoto2["url"];?>" alt="<?php echo $rowFoto2["idFoto"];?>">
                                                    </div>
                                                <?php } else {
                                                    $active = true;?>
                                                    <div class="item active imgcarousel">
                                                        <img class="" src="<?php echo $rowFoto2["url"];?>" alt="<?php echo $rowFoto2["idFoto"];?>">
                                                    </div>
                                                <?php }
                                            }
                                        }
                                        ?>
                                    </div>
                                    <!-- Left and right controls -->
                                    <a class="left carousel-control" href="#myCarousel<?php echo $rowPortfolio["id"]; ?>" role="button" data-slide="prev">
                                        <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                                        <span class="sr-only">Anterior</span>
                                    </a>
                                    <a class="right carousel-control" href="#myCarousel<?php echo $rowPortfolio["id"]; ?>" role="button" data-slide="next">
                                        <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                                        <span class="sr-only">Próximo</span>
                                    </a>
                                </div><br>
                                <p><?php echo $rowPortfolio["descricao"];?></p>
                                <button type="button" class="btn btn-primary" data-dismiss="modal"><i class="fa fa-times"></i> Fechar janela</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    <?php }
}
$conn->close();
?>




